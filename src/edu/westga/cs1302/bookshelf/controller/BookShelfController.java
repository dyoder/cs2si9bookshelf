package edu.westga.cs1302.bookshelf.controller;

import edu.westga.cs1302.bookshelf.model.Book;
import edu.westga.cs1302.bookshelf.model.BookShelf;

/**
 * The Class DemoController.
 * 
 * @author CS 1302
 */
public class BookShelfController {

	/**
	 * Demos functionality
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void run() {
		BookShelf shelf = new BookShelf();
		shelf.add(new Book("CS Rocks", "Yoder", "978-0131234567", 197, 12.99));
		shelf.add(new Book("Java Basics", "Remshagen", "978-0137654321", 250, 19.99));
		shelf.add(new Book("CS1302 Best Practices", "Remshagen", "978-0132234567", 328, 48.25));
		shelf.add(new Book("Web Design Principles", "Remshagen", "978-0130001112", 92, 3.99));
		shelf.add(new Book("CS Rules", "Yoder", "978-0132223334", 127, 5.99));
		shelf.add(new Book("Hunting Wabbits", "Fudd", "978-0138234", 528, 99.99));
		
		this.display(shelf.getBooks(), "Books on shelf:");
	}
	
	private <T> void display(Iterable<T> items, String description) {
		System.out.println();
		if (description != null && !description.isEmpty()) {
			System.out.println(description);
		}

		for (T currItem : items) {
			System.out.println(currItem);
		}
	}
	
}
