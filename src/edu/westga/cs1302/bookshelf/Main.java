package edu.westga.cs1302.bookshelf;

import edu.westga.cs1302.bookshelf.controller.BookShelfController;

/**
 * The Class Main.
 * 
 * @author CS 1302
 */
public class Main {

	/**
	 * Default entry point for the program.
	 * 
	 * @param args command line arguments for the program
	 */
	public static void main(String[] args) {
		BookShelfController demo = new BookShelfController();
		demo.run();
	}

}
