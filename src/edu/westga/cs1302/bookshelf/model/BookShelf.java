package edu.westga.cs1302.bookshelf.model;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The Class BookShelf.
 * 
 * @author CS1302
 */
public class BookShelf {
	
	private Collection<Book> books;
	
	/**
	 * Instantiates a new book shelf.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public BookShelf() {
		this.books = new ArrayList<Book>();
	}
	
	/**
	 * Adds the book to the shelf, only if book doesn't exist.
	 * 
	 * @precondition book != null
	 * @postcondition size() == size()@prev + 1
	 *
	 * @param book the book to add
	 * @return true, if successful added book, false if didn't add book to shelf
	 */
	public boolean add(Book book) {
		if (book == null) {
			throw new IllegalArgumentException("book cannot be null.");
		}
		
		if (this.findBook(book.getISBN()) != null) {
			return false;
		}
		
		return this.books.add(book);
	}
	
	/**
	 * Finds and returns book specified book on shelf.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param isbn the isbn
	 * @return the found book object or null if not found
	 */
	public Book findBook(String isbn) {
		for (Book currBook : this.books) {
			if (currBook.getISBN().equalsIgnoreCase(isbn)) {
				return currBook;
			}
		}
		
		return null;
	}
	
	/**
	 * Gets the collection of books.
	 *
	 * @return the books
	 */
	public Collection<Book> getBooks() {
		return this.books;
	}
	
	/**
	 * Number of books on shelf.
	 *
	 * @return the int
	 */
	public int size() {
		return this.books.size();
	}
	

}
