package edu.westga.cs1302.bookshelf.model;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * The Class Book.
 * 
 * @author CS 1302
 */
public class Book {

	private String name;
	private String author;
	private String isbn;
	private int pages;
	private double cost;

	/**
	 * Instantiates a new book from specified data.
	 * 
	 * @precondition name != null && !name.isEmpty() AND author != null &&
	 *               !author.isEmpty() AND isbn != null && !isbn.isEmpty() && isbn
	 *               is of the following format 978-013####### AND pages > 0 and
	 *               cost >= 0
	 * @postcondition getName() == name; getAuthor() == author; getIsbn() == isbn;
	 *                getPages() == pages; getCost() == cost
	 *
	 * @param name   the name
	 * @param author the author
	 * @param isbn   the isbn
	 * @param pages  the pages
	 * @param cost   the cost
	 */
	public Book(String name, String author, String isbn, int pages, double cost) {
		this.enforceNullandEmptyString(name, "name");
		this.enforceNullandEmptyString(author, "author");
		this.enforceNullandEmptyString(isbn, "isbn");

		// TODO Add code to enforce the required precondition format for the ISBN

		if (pages <= 0) {
			throw new IllegalArgumentException("pages must be > 0.");
		}

		if (cost < 0) {
			throw new IllegalArgumentException("cost must be >= 0.");
		}

		this.name = name;
		this.author = author;
		this.isbn = isbn;
		this.pages = pages;
		this.cost = cost;
	}

	/**
	 * Enforce nulland empty string.
	 *
	 * @param variable     the variable
	 * @param variableName the variable name
	 */
	private void enforceNullandEmptyString(String variable, String variableName) {
		if (variable == null) {
			throw new IllegalArgumentException(variableName + " cannot be null.");
		}

		if (variable.isEmpty()) {
			throw new IllegalArgumentException(variableName + " cannot be empty.");
		}

	}

	/**
	 * Gets the cost.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the cost
	 */
	public double getCost() {
		return this.cost;
	}

	/**
	 * Sets the cost.
	 *
	 * @param cost the new cost
	 * @precondition cost >= 0
	 * @postcondition getCost() == cost
	 */
	public void setCost(double cost) {
		if (cost < 0) {
			throw new IllegalArgumentException("cost must be >= 0.");
		}

		this.cost = cost;
	}

	/**
	 * Gets the name.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Gets the author.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the author
	 */
	public String getAuthor() {
		return this.author;
	}

	/**
	 * Gets the isbn.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the isbn
	 */
	public String getISBN() {
		return this.isbn;
	}

	/**
	 * Gets the pages.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the pages
	 */
	public int getPages() {
		return this.pages;
	}

	@Override
	public String toString() {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US); 
		return this.name + " by " + author + ", ISBN=" + isbn + ", pages=" + pages + ", " + currencyFormatter.format(cost);
	}

}
